#! /usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
import math
from scipy.optimize import fsolve, minimize 

# В лабораторной работе было выбрано в качестве критерия устойчивости отношение численности жертв к численности хищников на грани вымирания, равное N.
# Этот выбор мотивирован тем, что, на мой взгляд, для ученых больший интерес представляют именно "пиковые" ситуации, когда виду грозит вымирание.
N = 1.5

def dif_func(parameters):
    alpha, beta, gamma, delta = parameters
    
    x0 = 1
    y0 = 1

    # Находим особую точку — такое значение размеров популяции животных, когда обе популяции остаются неизменными и сбалансированными.
    # Если начальное условие не попадает в особую точку, фазовые кривые будут идти вокруг нее, образуя бесконечное циклическое колебание.
    # То есть количество особей одного вида в течение неограниченного количества времени будет расти, другого — падать, затем наоборот. 
    x_s = gamma / delta
    y_s = alpha / beta
    
    # Из математической модели Лотки-Вольтерры с помощью математических преобразований можно получить следующие уравнение
    c = beta * y0 + delta * x0 - alpha * np.log(np.abs(y0)) - gamma * np.log(np.abs(x0))
    
    def func_x(x):
        return beta * y_s + delta * x - alpha * np.log(np.abs(y_s)) - gamma * np.log(np.abs(x)) - c
    
    def func_y(y):
        return beta * y + delta * x_s - alpha * np.log(np.abs(y)) - gamma * np.log(np.abs(x_s)) - c

    # Находим количество жертв и хищников на пике вымирания (то есть минимальные значения).
    x_min = fsolve(func_x, 0.001)
    y_min = fsolve(func_y, 0.001)
    
    return np.abs(y_min / x_min - N)


class Biome(object):

  def __init__(self, pred_init=1, prey_init=5, tmax=10, dt=0.001):
    self._dt = dt
    self._n_iters = int(tmax / dt)
    # Intial state
    self.pred_init = pred_init
    self.prey_init = prey_init
    # Data arrays
    self.time = None
    self.prey = None
    self.pred = None

  def run(self, alpha, beta, delta, gamma):
    """
    Simulation settings:
        alpha - prey growth
        beta - prey mortality
        delta - predator growrh
        gamma - predator mortality
    """
    self.time = np.zeros(self._n_iters)
    self.prey = np.zeros(self._n_iters)
    self.pred = np.zeros(self._n_iters)
    self.prey[0] = self.prey_init
    self.pred[0] = self.pred_init
    # Lotka-Volterra equations
    for i in range(self._n_iters - 1):
      self.prey[i + 1] = self.prey[i] + self._dt * self.prey[i] * (alpha - self.pred[i] * beta)
      self.pred[i + 1] = self.pred[i] + self._dt * self.pred[i] * (delta * self.prey[i] - gamma)
    self.time[1:] = np.add.accumulate([self._dt] * (self._n_iters - 1))
    return self.time, self.prey, self.pred

  def plot(self):
    plt.xlabel('time')
    plt.ylabel('n')
    plt.plot(self.time, self.pred, label='predators', color='r')
    plt.plot(self.time, self.prey, label='preys', color='b')
    plt.legend()
    plt.grid()
    plt.show()

  def plot_cycle(self):
    plt.xlabel('n predators')
    plt.ylabel('n preys')
    plt.plot(self.pred, self.prey, color='black')
    plt.grid()
    plt.show()


def main():
    x0 = 1
    y0 = 1
    result = minimize(dif_func, [1,1,1,1])
    a, b, g, d = result.x
    biome = Biome(pred_init=y0, prey_init=x0, tmax=10, dt=0.001)
    biome.run(a, b, g, d)
    biome.plot()
    biome.plot_cycle()

if __name__ == "__main__":
    main()

